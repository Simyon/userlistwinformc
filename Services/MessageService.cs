﻿using System.Windows.Forms;
using Common.Constants;

namespace Services
{
    public static class MessageService
    {
        public static void ShowError(string errorText)
        {
            MessageBox.Show(errorText, 
                CaptionStringConstants.Error, 
                MessageBoxButtons.OK,
                MessageBoxIcon.Error, 
                MessageBoxDefaultButton.Button1);
        }

        public static void ShowInformation(string informationText)
        {
            MessageBox.Show(informationText,
                CaptionStringConstants.Information,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1);
        }

        public static DialogResult ShowQuestion(string questionText)
        {
            return MessageBox.Show(questionText,
                CaptionStringConstants.Question,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);
        }

        public static DialogResult ShowQuestionWithCancel(string questionText)
        {
            return MessageBox.Show(questionText,
                CaptionStringConstants.Question,
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);
        }
    }
}
