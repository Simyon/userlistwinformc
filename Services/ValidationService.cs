﻿using System.Drawing;
using System.Text.RegularExpressions;
using Common.Constants;

namespace Services
{
    public static class ValidationService
    {
        public static bool Validate(string propertyName, string value, out string errorText)
        {
            errorText = string.Empty;
            switch (propertyName)
            {
                case PropertyNameStringConstants.FirstName:
                    return ValidateUserProperty(value, RegexPatternStringConstants.UserName, out errorText);
                case PropertyNameStringConstants.LastName:
                    return ValidateUserProperty(value, RegexPatternStringConstants.UserName, out errorText);
                case PropertyNameStringConstants.TownName:
                    return ValidateUserProperty(value, RegexPatternStringConstants.UserName, out errorText);
                case PropertyNameStringConstants.StreetName:
                    return ValidateUserProperty(value, RegexPatternStringConstants.UserName, out errorText);
                case PropertyNameStringConstants.HouseNumber:
                    return ValidateUserProperty(value, RegexPatternStringConstants.HouseNumber, out errorText);
                case PropertyNameStringConstants.HouseNumberSecond:
                    return ValidateUserProperty(value, RegexPatternStringConstants.HouseNumberSecond, out errorText);
                case PropertyNameStringConstants.Email:
                    return ValidateUserProperty(value, RegexPatternStringConstants.Email, out errorText);
                case PropertyNameStringConstants.PhoneNumber:
                    return ValidateUserProperty(value, RegexPatternStringConstants.PhoneNumber, out errorText);
                default:
                    return true;
            }
        }

        private static bool ValidateUserProperty(string value, string pattern, out string errorText)
        {
            errorText = string.Empty;

            value = value.Trim();

            if (string.IsNullOrWhiteSpace(value))
            {
                errorText = ValidationStringConstant.MustBeNotEmpty;

                return false;
            }

            var reg = new Regex(pattern);

            if (!reg.IsMatch(value))
            {
                errorText = ValidationStringConstant.DoesntCorrespondToPattern;

                return false;
            }

            return true;
        }

        public static Color GetBackColorFromValidation(string value, string pattern)
        {
            value = value.Trim();
            var reg = new Regex(pattern);

            return !reg.IsMatch(value)
                ? ColorConstants.NotValidBackColor
                : ColorConstants.ValidBackColor;
        }


        public static bool IsCellValid(string value, string pattern)
        {
            value = value.Trim();
            
            var reg = new Regex(pattern);

            return reg.IsMatch(value);
        }

        public static string GetPatternForProperty(string propertyName)
        {
            switch (propertyName)
            {
                case PropertyNameStringConstants.FirstName:
                case PropertyNameStringConstants.LastName:
                case PropertyNameStringConstants.TownName:
                case PropertyNameStringConstants.StreetName:
                    return RegexPatternStringConstants.UserName;
                case PropertyNameStringConstants.HouseNumber:
                    return RegexPatternStringConstants.HouseNumber;
                case PropertyNameStringConstants.HouseNumberSecond:
                    return RegexPatternStringConstants.HouseNumberSecond;
                case PropertyNameStringConstants.Email:
                    return RegexPatternStringConstants.Email;
                case PropertyNameStringConstants.PhoneNumber:
                    return RegexPatternStringConstants.PhoneNumber;
                default:
                    return null;
            }
        }
    }
}
