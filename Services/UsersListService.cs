﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Constants;
using Common.Serializers;
using Data.Criteria;
using Data.Model;
using Data.ViewMode;

namespace Services
{
    public class UsersListService
    {
        #region Fields
        private readonly OpenFileDialog openFileDialog = new OpenFileDialog
        {
            Filter = CommonStringConstants.FileFilterConstant,
            Multiselect = false
        };

        private readonly SaveFileDialog saveFileDialog = new SaveFileDialog
        {
            Filter = CommonStringConstants.FileFilterConstant,
            OverwritePrompt = true
        };

        private string OpenedFullFileName { get; set; }

        private bool IsDataChanged { get; set; }

        private List<UserViewModelForGrid> Users { get; set; } = new List<UserViewModelForGrid>();

        public int UserCount => Users.Count;
        #endregion Fields

        #region PrivateMethods
        private void GetUsersListFromXmlFile(string fullFileName)
        {
            if (IsDataChanged)
            {
                var dResult = MessageService.ShowQuestion(QuestionStringConstants.TryToOpenNewMessageText);

                if (dResult == DialogResult.Yes)
                {
                    SaveUsersToFile(OpenedFullFileName);
                }

            }

            var tempUsers = GetUsersFromFile(fullFileName);

            if (CheckIdDuplicate(tempUsers))
            {
                throw new Exception(ExceptionStringConstants.NotUniqueIDs);
            }

            Users = tempUsers;

            IsDataChanged = false;
        }

        private static List<UserViewModelForGrid> GetUsersFromFile(string fullFileName)
        {
            return XmlFileWorker
                .ReadXml<List<UserModel>>(fullFileName)
                .Select(u => new UserViewModelForGrid(u))
                .ToList();
        }

        private static bool CheckIdDuplicate(IEnumerable<UserViewModelForGrid> result)
        {
            var groups = result
                .GroupBy(v => v.Id)
                .Where(g => g.Count() > 1);

            return groups.Any();
        }

        private void SaveUsersToFile(string fullFileName)
        {
            try
            {
                Users.Select(u => u.Model)
                    .ToList()
                    .WriteXml(fullFileName);

                IsDataChanged = false;
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }

        private List<UserViewModelForGrid> GetUsersByComponent(List<UserViewModelForGrid> users,
            StringCriteriaComponent component)
        {
            var value = component.Value.Trim();
            if (string.IsNullOrWhiteSpace(value))
            {
                return users;
            }

            if (!component.CaseSensitive)
            {
                value = value.ToLower();
            }

            var getParameter = GetFuncForGetPropertyAsString(component.Type);

            var result = users.Where(r =>
            {
                var parameter = getParameter(r).Trim();
                if (string.IsNullOrWhiteSpace(parameter))
                {
                    return false;
                }

                if (!component.CaseSensitive)
                {
                    parameter = parameter.ToLower();
                }

                return component.IsPart ? parameter.Contains(value) : parameter == value;
            }).ToList();

            return result;
        }

        private Func<UserViewModelForGrid, string> GetFuncForGetPropertyAsString(UserProreptyType type)
        {
            switch (type)
            {
                case UserProreptyType.Id:
                    return user => user.Id.ToString();
                case UserProreptyType.FirstName:
                    return user => user.FirstName;
                case UserProreptyType.LastName:
                    return user => user.LastName;
                case UserProreptyType.TownName:
                    return user => user.TownName;
                case UserProreptyType.StreetName:
                    return user => user.StreetName;
                case UserProreptyType.HouseNumber:
                    return user => user.HouseNumber;
                case UserProreptyType.HouseNumberSecond:
                    return user => user.HouseNumberSecond;
                case UserProreptyType.Email:
                    return user => user.Email;
                case UserProreptyType.PhoneNumber:
                    return user => user.PhoneNumber;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
        #endregion PrivateMethods

        #region PublicMethods
        public bool HasIdDuplicateWithCurrentUsers(IEnumerable<UserViewModelForGrid> result)
        {
            return CheckIdDuplicate(Users.Concat(result));
        }

        public List<UserViewModelForGrid> GetUsersByCriteria(UsersFilteringCriteria criteria)
        {
            var result = Users;

            try
            {
                foreach (var property in criteria.UserProperties)
                {
                    result = GetUsersByComponent(result, property);
                }
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }

            return result.ToList();
        }

        public void AddUser(UserViewModelForGrid user)
        {
            Users.Add(user);

            IsDataChanged = true;
        }

        public void AddUsers(IEnumerable<UserViewModelForGrid> user)
        {
            Users.AddRange(user);

            IsDataChanged = true;
        }

        public void RemoveUser(UserViewModelForGrid user)
        {
            try
            {
                Users.Remove(user);

                IsDataChanged = true;
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }

        public UserViewModelForGrid FindById(Guid userId)
        {
            return Users.First(u => u.Id == userId);
        }

        public void SortUsers(SortOrder order, string propertyName)
        {
            try
            {
                Func<UserViewModelForGrid, object> keySelector = u => u.GetType()
                    .GetProperty(propertyName)
                    .GetValue(u, null);

                Users = (order == SortOrder.Ascending
                    ? Users.OrderBy(keySelector)
                    : Users.OrderByDescending(keySelector))
                    .ToList();
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }

        public void OpenData(string fullFileName = null)
        {
            if (fullFileName == null)
            {
                var result = openFileDialog.ShowDialog();
                if (result != DialogResult.OK)
                {
                    return;
                }
                fullFileName = openFileDialog.FileName;
            }

            GetUsersFromXmlFile(fullFileName);
        }

        public void GetUsersFromXmlFile(string fullFileName)
        {
            if (string.IsNullOrWhiteSpace(fullFileName))
            {
                MessageService.ShowError("Uncorrect file name");

                return;
            }

            if (!File.Exists(fullFileName))
            {
                MessageService.ShowError("File not exist");

                return;
            }

            try
            {
                OpenedFullFileName = fullFileName;

                GetUsersListFromXmlFile(OpenedFullFileName);
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }

        public void SaveToNewFile()
        {
            var result = saveFileDialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                return;
            }

            var fullFileName = saveFileDialog.FileName;

            SaveUsersToFile(fullFileName);
        }

        public void SaveToOpenedFile()
        {
            SaveUsersToFile(OpenedFullFileName);
        }

        public List<UserViewModelForGrid> GetUsersFromNewFile()
        {
            var result = openFileDialog.ShowDialog();
            if (result != DialogResult.OK)
            {
                return null;
            }

            try
            {
                var fullFileName = openFileDialog.FileName;

                return GetUsersFromFile(fullFileName);
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);

                return null;
            }
        }

        public DialogResult OnExitWithoutSaving()
        {
            DialogResult result;
            if (!IsDataChanged)
            {
                result = MessageService.ShowQuestion("You are sure?") == DialogResult.Yes
                    ? DialogResult.No
                    : DialogResult.Cancel;
            }
            else
            {
                result = MessageService.ShowQuestionWithCancel(QuestionStringConstants.TryToCloseMessageText);
            }

            if (result == DialogResult.Yes)
            {
                SaveToNewFile();
            }

            return result;
        }
        #endregion PublicMethods
    }
}
