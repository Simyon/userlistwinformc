﻿namespace Data.Model
{
    public enum UserProreptyType
    {
        Id,
        FirstName,
        LastName,
        TownName,
        StreetName,
        HouseNumber,
        HouseNumberSecond,
        Email,
        PhoneNumber
    }
}
