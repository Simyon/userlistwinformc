﻿using System;

namespace Data.Model
{
    [Serializable]
    public class UserModel
    {
        public Guid Id { get; set; }

        public FullNameModel FullName { get; set; }

        public UserAddressModel Address { get; set; }

        public PhoneNumberModel PhoneNumber { get; set; }

        public EmailModel Email { get; set; }
    }
}
