﻿using System;
using System.Text.RegularExpressions;

namespace Data.Model
{
    [Serializable]
    public class PhoneNumberModel
    {
        public string Number { get; set; }

        public PhoneNumberModel()
        {
            
        }

        public PhoneNumberModel(string number)
        {
            var reg = new Regex("^[0-9 ]+$");

            if (!reg.IsMatch(number))
            {
                throw new ArgumentException(nameof(number));
            }

            Number = number;
        }
    }
}
