﻿using System;

namespace Data.Model
{
    [Serializable]
    public class FullNameModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public FullNameModel()
        {
            
        }

        public FullNameModel(string firstName, string LastName)
        {
            if (string.IsNullOrWhiteSpace(firstName))
            {
                throw new ArgumentException(nameof(firstName));
            }

            if (string.IsNullOrWhiteSpace(LastName))
            {
                throw new ArgumentException(nameof(LastName));
            }

            FirstName = firstName;
            LastName = LastName;
        }
    }
}
