﻿using System;
using System.Net.Mail;

namespace Data.Model
{
    [Serializable]
    public class EmailModel
    {
        public string Mail { get; set; }
      
        public EmailModel()
        {

        }

        public EmailModel(string fullEmail)
        {
            MailAddress mail;
            if (!IsValid(fullEmail, out mail))
            {
                throw new ArgumentException(nameof(fullEmail));
            }

            Mail = fullEmail;
        }

        public bool IsValid(string emailaddress, out MailAddress email)
        {
            email = null;

            try
            {
                email = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
