﻿using System;

namespace Data.Model
{
    [Serializable]
    public class UserAddressModel
    {
        public string TownName { get; set; }

        public string StreetName { get; set; }

        public string HouseNumber { get; set; }

        public string HouseNumberSecond { get; set; }
    }
}
