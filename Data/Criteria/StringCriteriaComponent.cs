﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Common.Constants;
using Data.Model;

namespace Data.Criteria
{
    public class StringCriteriaComponent : CriteriaComponent<string>
    {
        public UserProreptyType Type { get; private set; }
        public bool CaseSensitive { get; private set; }

        public StringCriteriaComponent(string command)
        {
            var aroundEqualeParts = command.Split(new[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
            if (aroundEqualeParts.Length != 2)
            {
                throw new Exception(command);
            }

            var parametersCommandPart = aroundEqualeParts.First();

            var parts = parametersCommandPart.Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);

            var length = parts.Length;
            if (length < 1 || length > 3)
            {
                throw new Exception(parametersCommandPart);
            }

            foreach (var part in parts)
            {
                if (string.IsNullOrWhiteSpace(part))
                {
                    throw new Exception(parametersCommandPart);
                }
            }

            var typeCommandPart = parts.First();
            Type = GetCommamdType(typeCommandPart);

            bool isPart;
            bool caseSensitive;
            var listOfParts = parts.ToList();
            listOfParts.RemoveAt(0);
            GetModificators(listOfParts, out isPart, out caseSensitive);

            IsPart = isPart;
            CaseSensitive = caseSensitive;

            var valueCommandPart = aroundEqualeParts.Last();
            if (string.IsNullOrWhiteSpace(valueCommandPart))
            {
                throw new Exception(command);
            }

            Value = valueCommandPart.Trim();
        }

        private static void GetModificators(IEnumerable<string> parts, out bool isPart, out bool caseSensitive)
        {
            isPart = false;
            caseSensitive = false;

            var isPartReg = new Regex(RegexPatternStringConstants.IsPartCommandPart);
            var caseSensetiveReg = new Regex(RegexPatternStringConstants.CaseSensetiveCommandPart);
            foreach (var part in parts)
            {
                if (isPartReg.IsMatch(part))
                {
                    if (caseSensetiveReg.IsMatch(part))
                    {
                        throw new Exception(part);
                    }

                    if (isPart)
                    {
                        throw new Exception(part);
                    }

                    isPart = true;
                }
                else if(caseSensetiveReg.IsMatch(part))
                {
                    if (caseSensitive)
                    {
                        throw new Exception(part);
                    }

                    caseSensitive = true;
                }
                else
                {
                    throw new Exception(part);
                }
            }
        }

        private static UserProreptyType GetCommamdType(string typeCommandPart)
        {
            var type = typeCommandPart
                .Trim()
                .ToLower();

            var reg = new Regex(RegexPatternStringConstants.FirstNameCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.FirstName;
            }

            reg = new Regex(RegexPatternStringConstants.LastNameCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.LastName;
            }

            reg = new Regex(RegexPatternStringConstants.TownNameCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.TownName;
            }

            reg = new Regex(RegexPatternStringConstants.StreetNameCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.StreetName;
            }

            reg = new Regex(RegexPatternStringConstants.HouseNumberCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.HouseNumber;
            }

            reg = new Regex(RegexPatternStringConstants.HouseNumberSecondCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.HouseNumberSecond;
            }

            reg = new Regex(RegexPatternStringConstants.PhoneNumberCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.PhoneNumber;
            }

            reg = new Regex(RegexPatternStringConstants.EmailCommandPart);
            if (reg.IsMatch(type))
            {
                return UserProreptyType.Email;
            }

            throw new Exception(ExceptionStringConstants.UncorrectCommand);
        }
    }
}
