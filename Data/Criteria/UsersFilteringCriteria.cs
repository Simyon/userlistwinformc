﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Common.Constants;

namespace Data.Criteria
{
    public class UsersFilteringCriteria
    {
        public UsersFilteringCriteria()
        {
            UserProperties = new List<StringCriteriaComponent>();
        }

        public UsersFilteringCriteria(string filterText, CancellationToken cancellationToken)
        {
            UserProperties = new List<StringCriteriaComponent>();

            var commands = filterText.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            var exceptions = new List<Exception>();
            foreach (var command in commands)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }

                try
                {
                    var component = new StringCriteriaComponent(command);

                    UserProperties.Add(component);
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
        }

        public UsersFilteringCriteria(string filterText, out List<Exception> exceptions)
        {
            UserProperties = new List<StringCriteriaComponent>();

            var commands = filterText.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            var reg = new Regex(RegexPatternStringConstants.FullCommand);

            var correctCommands = commands
                .AsParallel()
                .Where(c => reg.IsMatch(c))
                .ToArray();

            var thisExceptions = commands.AsParallel()
                .Where(c => !reg.IsMatch(c))
                .Select(c => new Exception(c))
                .ToList();
            
            var components = correctCommands
                .AsParallel()
                .Select(c =>
                {
                    try
                    {
                        return new StringCriteriaComponent(c);
                    }
                    catch (Exception ex)
                    {
                        thisExceptions.Add(ex);

                        return null;
                    }
                }).Where(c => c != null);

            UserProperties.AddRange(components);

            exceptions = thisExceptions;
        }

        public readonly List<StringCriteriaComponent> UserProperties;
    }
}