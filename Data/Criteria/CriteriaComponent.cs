﻿namespace Data.Criteria
{
    public abstract class CriteriaComponent<T>
    {
        public bool IsPart { get; set; }

        public T Value { get; set; }
    }
}
