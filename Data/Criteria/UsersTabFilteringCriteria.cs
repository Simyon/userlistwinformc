﻿namespace Data.Criteria
{
    public class UsersTabFilteringCriteria
    {
        public int RowCount { get; set; }

        public int TabIndex { get; set; }
    }
}
