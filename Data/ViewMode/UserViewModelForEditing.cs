﻿using Data.Model;

namespace Data.ViewMode
{
    public class UserViewModelForEditing
    {
        public UserModel Model { get; }

        public UserViewModelForEditing(UserModel model)
        {
            Model = model;
        }

        public string FirstName => Model.FullName?.FirstName;

        public string LastName => Model.FullName?.LastName;

        public string Mail => Model.Email?.Mail;

        public string Number => Model.PhoneNumber?.Number;

        public string Town => Model.Address?.TownName;

        public string Street => Model.Address?.StreetName;

        public string HouseNumber => Model.Address?.HouseNumber;

        public string HouseNumberSecond => Model.Address?.HouseNumberSecond;
    }
}
