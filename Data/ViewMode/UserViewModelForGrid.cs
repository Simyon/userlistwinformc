﻿using System;
using Common;
using Data.Model;

namespace Data.ViewMode
{
    public class UserViewModelForGrid : IDeepClonable<UserViewModelForGrid>
    {
        public UserModel Model { get; }

        public UserViewModelForGrid(UserModel model)
        {
            Model = model;
        }

        public Guid Id { get { return Model.Id; } set { Model.Id = value; } }

        public string FirstName { get { return Model.FullName?.FirstName; } set { Model.FullName.FirstName = value; } }
        
        public string LastName { get { return Model.FullName?.LastName; } set { Model.FullName.LastName = value; } }

        public string Email { get { return Model.Email?.Mail; } set { Model.Email.Mail = value; } }

        public string PhoneNumber { get { return Model.PhoneNumber?.Number; } set { Model.PhoneNumber.Number = value; } }

        public string TownName { get { return Model.Address?.TownName; } set { Model.Address.TownName = value; } }

        public string StreetName { get { return Model.Address?.StreetName; } set { Model.Address.StreetName = value; } }

        public string HouseNumber { get { return Model.Address?.HouseNumber; } set { Model.Address.HouseNumber = value; } }

        public string HouseNumberSecond { get { return Model.Address?.HouseNumberSecond; } set { Model.Address.HouseNumberSecond = value; } }

        public UserViewModelForGrid DeepClone() => IndependentOfTypeExtentions.DeepClone(this);
    }
}
