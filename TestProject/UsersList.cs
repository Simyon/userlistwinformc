﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Constants;
using Data.Criteria;
using Data.Model;
using Data.ViewMode;
using Services;

namespace TestProject
{
    public partial class UsersList : Form
    {
        #region Fields

        private bool isWritedByUser;

        private readonly UsersListService controller = new UsersListService();

        private readonly Dictionary<string, SortOrder> ordersDictionary = new Dictionary<string, SortOrder>();
        private UsersFilteringCriteria filteringCriteria = new UsersFilteringCriteria();
        private List<UserViewModelForGrid> FilteredUserList { get; set; } = new List<UserViewModelForGrid>();
        #endregion Fields

        public UsersList(string fullFileName, string fullFilterSettings)
        {
            InitializeComponent();

            usersDataGridView.AutoGenerateColumns = false;
            usersDataGridView.AllowUserToAddRows = false;

            rowCountComboBox.SelectedIndex = 1;
            tabIndexesComboBox.SelectedIndex = 0;

            for (var i = 0; i < 9; i++)
            {
                usersDataGridView.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                usersDataGridView.Columns[i].SortMode = DataGridViewColumnSortMode.Programmatic;
            }

            if (!string.IsNullOrWhiteSpace(fullFileName))
            {
                if (File.Exists(fullFileName))
                {
                    controller.GetUsersFromXmlFile(fullFileName);
                }
            }

            if (!string.IsNullOrWhiteSpace(fullFilterSettings))
            {
                isWritedByUser = true;
                filterTextBox.Text = fullFilterSettings;
                filterTextBox.SelectionIndent = filterTextBox.TextLength;
                filterTextBox_TextChanged(filterTextBox, new EventArgs());
            }
            else
            {
                WriteHint(filterTextBox);
            }

            UpdateUsersGrid();
        }

        #region GripUpdate
        private async void UpdateUsersGrid(bool shouldFilterAgain = true, bool shouldCheckTabCount = true)
        {
            if (shouldFilterAgain)
            {
                var task = Task.Run(() => controller.GetUsersByCriteria(filteringCriteria));

                FilteredUserList = await task;
            }

            var count = FilteredUserList.Count();

            if (shouldCheckTabCount)
            {
                SetTabIndexValue(count);
            }

            var usersTabFilteringCriteria = GetUsersTabFilteringCriteria();

            var resultUsers = GetUsersForTab(usersTabFilteringCriteria);

            usersDataGridView.CellValidating -= usersDataGridView_CellValidating;
            usersDataGridView.DataSource = resultUsers.ToList();
            usersDataGridView.CellValidating += usersDataGridView_CellValidating;

            ValidateAllGridByColor();

            usersDataGridView.ClearSelection();

            userCountToolStripStatusLabel.Text = CommonStringConstants.UserCount + CommonStringConstants.DaublePoint + count;
        }

        private IEnumerable<UserViewModelForGrid> GetUsersForTab(UsersTabFilteringCriteria criteria)
        {
            var tabIndex = criteria.TabIndex;
            var rowCount = criteria.RowCount;

            return FilteredUserList.Skip((tabIndex - 1) * rowCount)
                .Take(rowCount);
        }

        private void ValidateAllGridByColor()
        {
            foreach (DataGridViewRow column in usersDataGridView.Rows)
            {

                foreach (DataGridViewCell cell in column.Cells)
                {
                    var propertyName = cell.OwningColumn.DataPropertyName;
                    var pattern = ValidationService.GetPatternForProperty(propertyName);
                    if (string.IsNullOrWhiteSpace(pattern))
                    {
                        continue;
                    }

                    var value = cell.FormattedValue?.ToString();
                    if (!ValidationService.IsCellValid(value, pattern))
                    {
                        cell.Style.BackColor = ColorConstants.NotValidBackColor;
                    }
                }
            }
        }
        #endregion GripUpdate

        #region Criteria
        private UsersTabFilteringCriteria GetUsersTabFilteringCriteria()
        {
            var usersTabFilteringCriteria = new UsersTabFilteringCriteria
            {
                TabIndex = Convert.ToInt32(tabIndexesComboBox.SelectedItem),
                RowCount = Convert.ToInt32(rowCountComboBox.SelectedItem)
            };

            return usersTabFilteringCriteria;
        }
        #endregion Criteria

        #region GridClick
        private void usersDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }

            var grid = sender as DataGridView;
            if (grid == null)
            {
                return;
            }

            var button = grid.Columns[e.ColumnIndex] as DataGridViewButtonColumn;
            if (button == null)
            {
                return;
            }

            var userIdString = usersDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString();

            try
            {
                var userId = Guid.Parse(userIdString);
                var user = controller.FindById(userId);

                PerformAction(button.Name, user);
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }

        private void usersDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var grid = sender as DataGridView;
            if (grid == null)
            {
                return;
            }

            var column = grid.Columns[e.ColumnIndex];

            if (!(column.CellTemplate is DataGridViewTextBoxCell))
            {
                return;
            }

            var propertyName = column.DataPropertyName;

            const SortOrder ascendingOrder = SortOrder.Ascending;
            const SortOrder descendingOrder = SortOrder.Descending;

            SortOrder sortOrder;
            if (!ordersDictionary.ContainsKey(propertyName))
            {
                ordersDictionary.Add(propertyName, ascendingOrder);

                sortOrder = ascendingOrder;
            }
            else
            {
                sortOrder = ordersDictionary[propertyName] == ascendingOrder
                    ? descendingOrder
                    : ascendingOrder;

                ordersDictionary[propertyName] = sortOrder;
            }

            if (controller.UserCount > 0)
            {
                controller.SortUsers(sortOrder, propertyName);

                UpdateUsersGrid(shouldCheckTabCount: false);

                column.HeaderCell.SortGlyphDirection = sortOrder;
            }
        }
        #endregion GridClick

        #region RowComboBox
        private void rowCountComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateUsersGrid(false);
        }

        private void SetTabIndexValue(int count)
        {
            var rowCount = Convert.ToInt32(rowCountComboBox.SelectedItem.ToString());

            var tabCount = (count / rowCount) + ((count % rowCount) == 0 ? 0 : 1);
            if (tabCount < 1)
            {
                return;
            }

            var index = Convert.ToInt32(tabIndexesComboBox.SelectedItem);
            if (index < 1)
            {
                index = 1;
            }

            var items = GetIndexes(tabCount).ToArray();

            tabIndexesComboBox.Items.Clear();
            tabIndexesComboBox.Items.AddRange(items);

            if (index > tabCount)
            {
                tabIndexesComboBox.SelectedIndex = tabIndexesComboBox.Items.Count - 1;
            }
            else
            {
                tabIndexesComboBox.SelectedIndex = index - 1;
            }

            tabCountToolStripStatusLabel.Text = CommonStringConstants.TabCount + CommonStringConstants.DaublePoint + tabCount;
        }

        private static IEnumerable<object> GetIndexes(int tabCount)
        {
            for (var i = 0; i < tabCount; i++)
            {
                yield return (i + 1).ToString();
            }
        }
        #endregion RowComboBox

        #region TabControl
        private void followingTabButton_Click(object sender, EventArgs e)
        {
            var item = Convert.ToInt32(tabIndexesComboBox.SelectedIndex);
            if (item >= tabIndexesComboBox.Items.Count - 1)
            {
                return;
            }

            tabIndexesComboBox.SelectedIndex = item + 1;

            UpdateUsersGrid(false, false);
        }

        private void previousTabButton_Click(object sender, EventArgs e)
        {
            var item = Convert.ToInt32(tabIndexesComboBox.SelectedIndex);
            if (item <= 0)
            {
                return;
            }

            tabIndexesComboBox.SelectedIndex = item - 1;

            UpdateUsersGrid(false, false);
        }

        private void tabIndexesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateUsersGrid(false, false);
        }
        #endregion TabControl

        #region ContextMenu
        private void usersDataGridView_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                return;
            }

            var gridView = sender as DataGridView;
            if (gridView == null)
            {
                return;
            }

            var menu = new ContextMenuStrip();

            var selectedRowIndex = gridView.HitTest(e.X, e.Y).RowIndex;
            if (selectedRowIndex >= 0)
            {
                menu.Items.Add("Create New From This").Name = CommandStringConstants.CreateFromThis;
                menu.Items.Add("Create").Name = CommandStringConstants.Create;
                menu.Items.Add("-");
                menu.Items.Add("Edit").Name = CommandStringConstants.Edit;
                menu.Items.Add("Delete").Name = CommandStringConstants.Delete;
                menu.Items.Add("-");
                RowSelect(gridView, selectedRowIndex);
            }

            menu.Items.Add("Open").Name = CommandStringConstants.Open;
            menu.Items.Add("Save").Name = CommandStringConstants.Save;
            menu.Items.Add("-");
            menu.Items.Add("Import").Name = CommandStringConstants.Import;
            menu.Items.Add("Export").Name = CommandStringConstants.Export;

            menu.Show(gridView, new Point(e.X, e.Y));

            menu.ItemClicked += (o, args) =>
            {
                MenuOnItemClicked(menu, args.ClickedItem.Name, selectedRowIndex);
            };

            gridView.Click += (o, args) => RowUnSelect(gridView, selectedRowIndex);
        }

        private static void RowSelect(DataGridView gridView, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return;
            }

            if (gridView.RowCount < 1)
            {
                return;
            }

            gridView.ReadOnly = true;
            gridView.ClearSelection();
            gridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            gridView.Rows[rowIndex].Selected = true;
        }

        private static void RowUnSelect(DataGridView gridView, int rowIndex)
        {
            gridView.ReadOnly = false;

            gridView.Click -= (o, args) => { RowUnSelect(gridView, rowIndex); };

            if (rowIndex < 0)
            {
                return;
            }

            if (gridView.RowCount < 1 || gridView.RowCount < rowIndex)
            {
                return;
            }

            gridView.Rows[rowIndex].Selected = false;
            gridView.SelectionMode = DataGridViewSelectionMode.CellSelect;
            gridView.ClearSelection();
        }

        private void MenuOnItemClicked(ToolStripDropDown menu, string itemName, int selectedRowIndex)
        {
            menu.Close();

            try
            {
                var user = new UserViewModelForGrid(new UserModel());
                if (selectedRowIndex >= 0)
                {
                    var row = usersDataGridView.Rows[selectedRowIndex];

                    var userIdString = row.Cells[0].Value.ToString();

                    var userId = Guid.Parse(userIdString);

                    user = controller.FindById(userId);
                }

                PerformAction(itemName, user);
            }
            catch (Exception ex)
            {
                MessageService.ShowError(ex.Message);
            }
        }
        #endregion ContextMenu

        #region MenuEvents
        private void UsersList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (controller.OnExitWithoutSaving() == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void ImportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Import();
        }

        private void ExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Export();
        }

        private void ExitAndSaveChangesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controller.SaveToNewFile();

            CloseUserList();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseUserList();
        }

        private void CloseUserList()
        {
            FormClosing -= UsersList_FormClosing;

            Close();
        }
        #endregion MenuEvents

        #region ActionPerform
        private void PerformAction(string itemName, UserViewModelForGrid user)
        {
            bool somethingChanged;
            switch (itemName)
            {
                case CommandStringConstants.CreateFromThis:
                    somethingChanged = CreateNewFromSelectedRow(user);
                    break;
                case CommandStringConstants.Create:
                    somethingChanged = CreateNew();
                    break;
                case CommandStringConstants.Edit:
                    somethingChanged = EditSelectedRow(user);
                    break;
                case CommandStringConstants.Delete:
                    somethingChanged = DeleteSelectedRow(user);
                    break;
                case CommandStringConstants.Open:
                    Open();
                    somethingChanged = true;
                    break;
                case CommandStringConstants.Save:
                    Save();
                    somethingChanged = false;
                    break;
                case CommandStringConstants.Import:
                    Import();
                    somethingChanged = true;
                    break;
                case CommandStringConstants.Export:
                    Export();
                    somethingChanged = false;
                    break;
                default:
                    somethingChanged = false;
                    break;
            }

            if (!somethingChanged)
            {
                return;
            }

            UpdateUsersGrid();
        }
        private void Open()
        {
            controller.OpenData();

            UpdateUsersGrid();
        }

        private void Save()
        {
            controller.SaveToOpenedFile();
        }

        private void Import()
        {
            var newUsers = controller.GetUsersFromNewFile();
            if (newUsers == null)
            {
                return;
            }

            if (!newUsers.Any())
            {
                MessageService.ShowInformation(InfermationStringConstant.NoDataInFile);

                return;
            }

            var hasDuplicate = controller.HasIdDuplicateWithCurrentUsers(newUsers);
            if (!hasDuplicate)
            {
                controller.AddUsers(newUsers);

                UpdateUsersGrid();
            }
            else
            {
                MessageService.ShowError(ExceptionStringConstants.DuplicatedIdentifiers);
            }
        }

        private void Export()
        {
            controller.SaveToNewFile();
        }

        private bool CreateNewFromSelectedRow(UserViewModelForGrid userView)
        {
            var deepClone = userView.DeepClone();
            deepClone.Id = Guid.NewGuid();

            var form = new UserSettings(deepClone.Model) { Owner = this };

            var result = form.ShowDialog();
            if (result != DialogResult.OK)
            {
                return false;
            }

            controller.AddUser(new UserViewModelForGrid(deepClone.Model));

            return true;
        }

        private bool CreateNew()
        {
            var id = Guid.NewGuid();

            var user = new UserModel { Id = id };

            var form = new UserSettings(user) { Owner = this };

            var result = form.ShowDialog();
            if (result != DialogResult.OK)
            {
                return false;
            }

            controller.AddUser(new UserViewModelForGrid(user));

            return true;
        }

        private bool DeleteSelectedRow(UserViewModelForGrid userView)
        {
            var result = MessageService.ShowQuestion(QuestionStringConstants.TryToDeleteMessageText);

            if (result != DialogResult.Yes)
            {
                return false;
            }

            controller.RemoveUser(userView);

            return true;
        }

        private bool EditSelectedRow(UserViewModelForGrid user)
        {
            controller.RemoveUser(user);

            var form = new UserSettings(user.Model) { Owner = this };

            controller.AddUser(user);

            var result = form.ShowDialog();

            return result == DialogResult.OK;
        }
        #endregion ActionPerform

        #region GridCellsValidation
        private void usersDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            var grid = sender as DataGridView;
            if (grid == null)
            {
                return;
            }

            var propertyName = grid.Columns[e.ColumnIndex].DataPropertyName;
            var value = e.FormattedValue.ToString();

            string errorText;
            var isValid = ValidationService.Validate(propertyName, value, out errorText);

            if (!isValid)
            {
                grid.Rows[e.RowIndex].ErrorText = errorText;
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = ColorConstants.NotValidBackColor;
                e.Cancel = true;
            }
            else
            {
                grid.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = ColorConstants.ValidBackColor;
            }
        }

        private void usersDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var grid = sender as DataGridView;
            if (grid == null)
            {
                return;
            }

            grid.Rows[e.RowIndex].ErrorText = string.Empty;
        }

        private void usersDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var txtBox = e.Control as TextBox;

            var propertyName = usersDataGridView.CurrentCell.OwningColumn.DataPropertyName;
            var pattern = ValidationService.GetPatternForProperty(propertyName);

            if (txtBox != null)
            {
                txtBox.TextChanged += (o, args) =>
                {
                    var textBox = o as TextBox;
                    if (textBox == null)
                    {
                        return;
                    }

                    textBox.BackColor = ValidationService.GetBackColorFromValidation(textBox.Text, pattern);
                };
            }
        }
        #endregion GridCellsValidation

        #region TextFilter
        private void filterTextBox_TextChanged(object sender, EventArgs e)
        {
            var txtBox = sender as RichTextBox;
            if (txtBox == null)
            {
                return;
            }

            var currentPos = txtBox.SelectionStart;
            var selectionLength = txtBox.SelectionLength;

            var text = txtBox.Text.Trim();

            if (isWritedByUser)
            {
                List<Exception> exceptions;
                filteringCriteria = new UsersFilteringCriteria(text, out exceptions);

                txtBox.Select(0, txtBox.TextLength);
                txtBox.SelectionColor = ColorConstants.CompleteCommandForeColor;
                txtBox.SelectionFont = new Font(txtBox.Font, FontStyle.Regular);
                txtBox.Select(currentPos, selectionLength);


                var errorWords = exceptions
                    .Select(ex => ex.Message)
                    .ToList();

                SetInvalidCommands(filterTextBox, errorWords, ColorConstants.NotValidBackColor);
                txtBox.Select(currentPos, selectionLength);

                //foreach (var error in exceptions)
                //{
                //    SetInvalidCommands(filterTextBox, error.Message, ColorConstants.NotValidBackColor);
                //    txtBox.Select(currentPos, selectionLength);
                //}

                if (isAutoCompliteShow)
                {
                    var length = autoComplitReplacement.Length;

                    if (length > 0)
                    {
                        SetInvalidCommands(filterTextBox, autoComplitReplacement, ColorConstants.OnAutoComplete);
                        txtBox.Select(currentPos, selectionLength);
                    }
                }

                if (controller.UserCount < 1)
                {
                    return;
                }

                if (!text.Any() || text.Last() == ';')
                {
                    UpdateUsersGrid();
                }
            }

            isWritedByUser = !string.IsNullOrWhiteSpace(text);
        }
        private static void SetInvalidCommands(RichTextBox txtBox, IReadOnlyList<string> errorWords, Color c)
        {
            var text = txtBox.Text;
            var end = txtBox.Text.Length - 1;

            for (var i = errorWords.Count - 1; i >= 0; i--)
            {
                var errorWord = errorWords[i];
                var at = text.LastIndexOf(errorWord, end, StringComparison.Ordinal);
                if (at <= -1)
                {
                    break;
                }

                txtBox.Select(at, errorWord.Length);
                txtBox.SelectionColor = c;
                txtBox.SelectionFont = new Font(txtBox.Font, FontStyle.Regular);

                end = at;
            }

            txtBox.DeselectAll();
        }

        private static void SetInvalidCommands(RichTextBox txtBox, string textWord, Color c)
        {
            var end = txtBox.Text.Length - 1;
            var at = txtBox.Text.LastIndexOf(textWord, end, StringComparison.Ordinal);
            if (at > -1)
            {
                txtBox.Select(at, textWord.Length);
                txtBox.SelectionColor = c;
                txtBox.SelectionFont = new Font(txtBox.Font, FontStyle.Regular);
                txtBox.DeselectAll();
            }
        }

        private void filterTextBox_Leave(object sender, EventArgs e)
        {
            if (isWritedByUser)
            {
                return;
            }

            var txtBox = sender as RichTextBox;
            if (txtBox == null)
            {
                return;
            }

            WriteHint(txtBox);
        }

        private void WriteHint(Control txtBox)
        {
            txtBox.ForeColor = ColorConstants.HintForeColor;
            txtBox.Text = CommonStringConstants.Hint;
            isWritedByUser = false;
        }

        private void filterTextBox_Enter(object sender, EventArgs e)
        {
            var txtBox = sender as RichTextBox;
            if (txtBox == null)
            {
                return;
            }

            if (!isWritedByUser)
            {
                txtBox.ForeColor = ColorConstants.NormalForeColor;
                txtBox.Text = string.Empty;
            }
        }
        #endregion TextFilter

        #region AutoComplit
        bool isAutoCompliteShow;
        string autoComplitReplacement = "";

        private void filterTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            var txtBox = sender as RichTextBox;
            if (txtBox == null)
            {
                return;
            }

            if (e.KeyChar == ';')
            {
                autoComplitReplacement = "";
            }

            if (e.KeyChar == ' ' && ModifierKeys == Keys.Control)
            {
                e.Handled = true;

                isWritedByUser = true;
                isAutoCompliteShow = true;

                ListBoxMove(txtBox);
                listBox1.Show();
                listBox1.SelectedIndex = listBox1.FindString(autoComplitReplacement);

                return;
            }

            if (!isAutoCompliteShow)
            {
                return;
            }

            var length = autoComplitReplacement.Length;

            if (e.KeyChar == 8)
            {
                if (txtBox.SelectionIndent != txtBox.TextLength)
                {
                    autoComplitReplacement = "";
                }
                else if (length > 0)
                {
                    autoComplitReplacement = autoComplitReplacement.Remove(length - 1, 1);
                }
            }
            else
            {
                autoComplitReplacement += e.KeyChar;

                listBox1.SelectedIndex = listBox1.FindString(autoComplitReplacement);
            }

            SetListBoxAlternatives();
            ListBoxMove(txtBox);
        }

        private void SetListBoxAlternatives()
        {
            var first = ArraStringConstants.AutoCompliteVariants
                .Where(a => a.Contains(autoComplitReplacement))
                .OrderBy(s => s)
                .ToList();

            var second = ArraStringConstants.AutoCompliteVariantsPart
                .Where(a => a.Contains(autoComplitReplacement))
                .OrderBy(s => s)
                .ToList();

            var enumerable = first.Concat(second).ToList();

            listBox1.DataSource = enumerable.Any()
                ? enumerable
                : ArraStringConstants.AutoCompliteVariants
                    .Concat(ArraStringConstants.AutoCompliteVariantsPart)
                    .ToList();
        }

        private void filterTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!isAutoCompliteShow)
            {
                return;
            }

            var txtBox = sender as RichTextBox;
            if (txtBox == null)
            {
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.Tab:
                    break;
                case Keys.Enter:
                    AutoAddText();
                    break;
                case Keys.Up:
                    listBox1.Focus();

                    if (listBox1.SelectedIndex > 0)
                    {
                        listBox1.SelectedIndex -= 1;
                    }

                    txtBox.Focus();
                    break;
                case Keys.Down:
                    listBox1.Focus();

                    var count = listBox1.Items.Count;
                    if (listBox1.SelectedIndex < count - 1)
                    {
                        listBox1.SelectedIndex += 1;
                    }

                    txtBox.Focus();
                    break;
                case Keys.Escape:
                    autoComplitReplacement = "";
                    isAutoCompliteShow = false;
                    listBox1.Hide();
                    break;
                case Keys.Left:
                case Keys.Right:
                    ListBoxMove(txtBox);
                    autoComplitReplacement = "";
                    return;
                default:
                    return;
            }

            e.Handled = true;
        }

        private void ListBoxMove(TextBoxBase txtBox)
        {
            var point = txtBox.GetPositionFromCharIndex(txtBox.SelectionStart);
            point.Y += (int)Math.Ceiling(txtBox.Font.GetHeight()) + 40;
            point.X += 20;
            listBox1.Location = point;
        }

        private void listBox1_MouseClick(object sender, MouseEventArgs e)
        {
            AutoAddText();
        }

        private void AutoAddText()
        {
            if (listBox1.SelectedItem == null)
            {
                return;
            }

            var autoText = listBox1.SelectedItem.ToString();

            isWritedByUser = true;

            var length = autoComplitReplacement.Length;

            var textLangth = filterTextBox.Text.Length;
            if (length > textLangth)
            {
                length = textLangth;
            }

            var beginPlace = filterTextBox.SelectionStart;
            var startIndex = beginPlace - length;
            if (startIndex < 0)
            {
                startIndex = 0;
            }

            if (length > 0)
            {
                var text = filterTextBox.Text
                    .Remove(startIndex, length)
                    .Insert(startIndex, autoText);

                filterTextBox.Text = text;
                filterTextBox.Focus();
            }
            else
            {
                filterTextBox.Text += autoText;
            }

            isAutoCompliteShow = false;
            autoComplitReplacement = "";
            listBox1.Hide();

            var endPlace = startIndex + autoText.Length;
            filterTextBox.SelectionStart = endPlace;
        }

        private void listBox1_VisibleChanged(object sender, EventArgs e)
        {
            var list = sender as ListBox;
            if (list == null)
            {
                return;
            }

            if (isAutoCompliteShow)
            {
                BringToFront();
            }
        }
        #endregion AutoComplit
    }
}