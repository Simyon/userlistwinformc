﻿using System.ComponentModel;
using System.Windows.Forms;

namespace TestProject
{
    partial class UsersList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.usersDataGridView = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Town = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Street = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HouseNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HouseNumberSecond = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.importDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitAndSaveChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FiltersGroupBox = new System.Windows.Forms.GroupBox();
            this.filterTextBox = new System.Windows.Forms.RichTextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabIndexesComboBox = new System.Windows.Forms.ComboBox();
            this.followingTabButton = new System.Windows.Forms.Button();
            this.previousTabButton = new System.Windows.Forms.Button();
            this.tabIndexLabel = new System.Windows.Forms.Label();
            this.rowCountComboBox = new System.Windows.Forms.ComboBox();
            this.rowCountLabel = new System.Windows.Forms.Label();
            this.usersGroupBox = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.userCountToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabCountToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.FiltersGroupBox.SuspendLayout();
            this.usersGroupBox.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // usersDataGridView
            // 
            this.usersDataGridView.AllowUserToOrderColumns = true;
            this.usersDataGridView.AllowUserToResizeColumns = false;
            this.usersDataGridView.AllowUserToResizeRows = false;
            this.usersDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usersDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.usersDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.firstNameDataGridViewTextBoxColumn,
            this.LastNameDataGridViewTextBoxColumn,
            this.mailDataGridViewTextBoxColumn,
            this.numberDataGridViewTextBoxColumn,
            this.Town,
            this.Street,
            this.HouseNumber,
            this.HouseNumberSecond,
            this.Edit,
            this.Delete});
            this.usersDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.usersDataGridView.Location = new System.Drawing.Point(6, 19);
            this.usersDataGridView.MultiSelect = false;
            this.usersDataGridView.Name = "usersDataGridView";
            this.usersDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.usersDataGridView.Size = new System.Drawing.Size(672, 293);
            this.usersDataGridView.TabIndex = 0;
            this.usersDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.usersDataGridView_CellContentClick);
            this.usersDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.usersDataGridView_CellEndEdit);
            this.usersDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.usersDataGridView_CellValidating);
            this.usersDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.usersDataGridView_ColumnHeaderMouseClick);
            this.usersDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.usersDataGridView_EditingControlShowing);
            this.usersDataGridView.MouseClick += new System.Windows.Forms.MouseEventHandler(this.usersDataGridView_MouseClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "Id";
            this.ID.HeaderText = "ID";
            this.ID.MaxInputLength = 10;
            this.ID.Name = "ID";
            this.ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.ID.Visible = false;
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "FirstName";
            this.firstNameDataGridViewTextBoxColumn.FillWeight = 82.08122F;
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "First Name";
            this.firstNameDataGridViewTextBoxColumn.MaxInputLength = 30;
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            this.firstNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.firstNameDataGridViewTextBoxColumn.Width = 90;
            // 
            // LastNameDataGridViewTextBoxColumn
            // 
            this.LastNameDataGridViewTextBoxColumn.DataPropertyName = "LastName";
            this.LastNameDataGridViewTextBoxColumn.FillWeight = 82.08122F;
            this.LastNameDataGridViewTextBoxColumn.HeaderText = "Last Name";
            this.LastNameDataGridViewTextBoxColumn.MaxInputLength = 30;
            this.LastNameDataGridViewTextBoxColumn.Name = "LastNameDataGridViewTextBoxColumn";
            this.LastNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.LastNameDataGridViewTextBoxColumn.Width = 89;
            // 
            // mailDataGridViewTextBoxColumn
            // 
            this.mailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.mailDataGridViewTextBoxColumn.FillWeight = 82.08122F;
            this.mailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.mailDataGridViewTextBoxColumn.MaxInputLength = 256;
            this.mailDataGridViewTextBoxColumn.Name = "mailDataGridViewTextBoxColumn";
            this.mailDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.mailDataGridViewTextBoxColumn.Width = 90;
            // 
            // numberDataGridViewTextBoxColumn
            // 
            this.numberDataGridViewTextBoxColumn.DataPropertyName = "PhoneNumber";
            this.numberDataGridViewTextBoxColumn.FillWeight = 82.08122F;
            this.numberDataGridViewTextBoxColumn.HeaderText = "Phone Number";
            this.numberDataGridViewTextBoxColumn.MaxInputLength = 10;
            this.numberDataGridViewTextBoxColumn.Name = "numberDataGridViewTextBoxColumn";
            this.numberDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.numberDataGridViewTextBoxColumn.Width = 89;
            // 
            // Town
            // 
            this.Town.DataPropertyName = "TownName";
            this.Town.HeaderText = "Town Name";
            this.Town.MaxInputLength = 30;
            this.Town.Name = "Town";
            this.Town.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Street
            // 
            this.Street.DataPropertyName = "StreetName";
            this.Street.HeaderText = "Street Name";
            this.Street.MaxInputLength = 30;
            this.Street.Name = "Street";
            this.Street.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // HouseNumber
            // 
            this.HouseNumber.DataPropertyName = "HouseNumber";
            this.HouseNumber.HeaderText = "House Number";
            this.HouseNumber.MaxInputLength = 5;
            this.HouseNumber.Name = "HouseNumber";
            this.HouseNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // HouseNumberSecond
            // 
            this.HouseNumberSecond.DataPropertyName = "HouseNumberSecond";
            this.HouseNumberSecond.HeaderText = "House Number Second";
            this.HouseNumberSecond.MaxInputLength = 3;
            this.HouseNumberSecond.Name = "HouseNumberSecond";
            this.HouseNumberSecond.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // Edit
            // 
            this.Edit.FillWeight = 50F;
            this.Edit.HeaderText = "Edit";
            this.Edit.Name = "Edit";
            this.Edit.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Edit.Text = "Edit";
            this.Edit.ToolTipText = "EDIT THIS USER";
            this.Edit.UseColumnTextForButtonValue = true;
            this.Edit.Width = 50;
            // 
            // Delete
            // 
            this.Delete.FillWeight = 50F;
            this.Delete.HeaderText = "Delete";
            this.Delete.Name = "Delete";
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Delete.Text = "Delete";
            this.Delete.ToolTipText = "DELETE THIS USER";
            this.Delete.UseColumnTextForButtonValue = true;
            this.Delete.Width = 50;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openDataToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripSeparator2,
            this.importDataToolStripMenuItem,
            this.exportDataToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitAndSaveChangesToolStripMenuItem,
            this.exitChangesToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openDataToolStripMenuItem
            // 
            this.openDataToolStripMenuItem.Name = "openDataToolStripMenuItem";
            this.openDataToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.openDataToolStripMenuItem.Text = "Open";
            this.openDataToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
            // 
            // importDataToolStripMenuItem
            // 
            this.importDataToolStripMenuItem.Name = "importDataToolStripMenuItem";
            this.importDataToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.importDataToolStripMenuItem.Text = "Import";
            this.importDataToolStripMenuItem.Click += new System.EventHandler(this.ImportToolStripMenuItem_Click);
            // 
            // exportDataToolStripMenuItem
            // 
            this.exportDataToolStripMenuItem.Name = "exportDataToolStripMenuItem";
            this.exportDataToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.exportDataToolStripMenuItem.Text = "Export";
            this.exportDataToolStripMenuItem.Click += new System.EventHandler(this.ExportToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // exitAndSaveChangesToolStripMenuItem
            // 
            this.exitAndSaveChangesToolStripMenuItem.Name = "exitAndSaveChangesToolStripMenuItem";
            this.exitAndSaveChangesToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.exitAndSaveChangesToolStripMenuItem.Text = "Save and Exit";
            this.exitAndSaveChangesToolStripMenuItem.Click += new System.EventHandler(this.ExitAndSaveChangesToolStripMenuItem_Click);
            // 
            // exitChangesToolStripMenuItem
            // 
            this.exitChangesToolStripMenuItem.Name = "exitChangesToolStripMenuItem";
            this.exitChangesToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.exitChangesToolStripMenuItem.Text = "Exit";
            this.exitChangesToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // FiltersGroupBox
            // 
            this.FiltersGroupBox.Controls.Add(this.filterTextBox);
            this.FiltersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FiltersGroupBox.Location = new System.Drawing.Point(0, 0);
            this.FiltersGroupBox.Name = "FiltersGroupBox";
            this.FiltersGroupBox.Size = new System.Drawing.Size(684, 60);
            this.FiltersGroupBox.TabIndex = 2;
            this.FiltersGroupBox.TabStop = false;
            this.FiltersGroupBox.Text = "Filters";
            // 
            // filterTextBox
            // 
            this.filterTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.filterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.filterTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.filterTextBox.Location = new System.Drawing.Point(6, 16);
            this.filterTextBox.Name = "filterTextBox";
            this.filterTextBox.Size = new System.Drawing.Size(672, 38);
            this.filterTextBox.TabIndex = 0;
            this.filterTextBox.Text = "";
            this.filterTextBox.TextChanged += new System.EventHandler(this.filterTextBox_TextChanged);
            this.filterTextBox.Enter += new System.EventHandler(this.filterTextBox_Enter);
            this.filterTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.filterTextBox_KeyDown);
            this.filterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.filterTextBox_KeyPress);
            this.filterTextBox.Leave += new System.EventHandler(this.filterTextBox_Leave);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "f",
            "fn",
            "f",
            "first",
            "firstname",
            "first",
            "l",
            "ln",
            "l",
            "last",
            "lastname",
            "t",
            "tn",
            "town",
            "townname",
            "s",
            "sn",
            "street",
            "streetname",
            "n",
            "name",
            "p",
            "pn",
            "phone",
            "phonenumber",
            "e",
            "email",
            "h",
            "hn",
            "hns",
            "hs",
            "house",
            "housenumber",
            "housenumbersecond",
            "number",
            "second",
            ".p",
            ".part",
            ".c",
            ".cs",
            ".c s",
            ".case",
            ".casesensetive",
            ".case sensetive"});
            this.listBox1.Location = new System.Drawing.Point(30, 60);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 108);
            this.listBox1.TabIndex = 1;
            this.listBox1.TabStop = false;
            this.listBox1.Visible = false;
            this.listBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseClick);
            this.listBox1.VisibleChanged += new System.EventHandler(this.listBox1_VisibleChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // tabIndexesComboBox
            // 
            this.tabIndexesComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tabIndexesComboBox.DisplayMember = "1";
            this.tabIndexesComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tabIndexesComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.tabIndexesComboBox.FormattingEnabled = true;
            this.tabIndexesComboBox.Items.AddRange(new object[] {
            "1"});
            this.tabIndexesComboBox.Location = new System.Drawing.Point(387, 319);
            this.tabIndexesComboBox.MaxDropDownItems = 100;
            this.tabIndexesComboBox.Name = "tabIndexesComboBox";
            this.tabIndexesComboBox.Size = new System.Drawing.Size(50, 21);
            this.tabIndexesComboBox.TabIndex = 16;
            this.tabIndexesComboBox.ValueMember = "1";
            this.tabIndexesComboBox.SelectedIndexChanged += new System.EventHandler(this.tabIndexesComboBox_SelectedIndexChanged);
            // 
            // followingTabButton
            // 
            this.followingTabButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.followingTabButton.Location = new System.Drawing.Point(435, 318);
            this.followingTabButton.Name = "followingTabButton";
            this.followingTabButton.Size = new System.Drawing.Size(23, 23);
            this.followingTabButton.TabIndex = 15;
            this.followingTabButton.Text = ">";
            this.followingTabButton.UseVisualStyleBackColor = true;
            this.followingTabButton.Click += new System.EventHandler(this.followingTabButton_Click);
            // 
            // previousTabButton
            // 
            this.previousTabButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.previousTabButton.Location = new System.Drawing.Point(366, 318);
            this.previousTabButton.Name = "previousTabButton";
            this.previousTabButton.Size = new System.Drawing.Size(23, 23);
            this.previousTabButton.TabIndex = 14;
            this.previousTabButton.Text = "<";
            this.previousTabButton.UseVisualStyleBackColor = true;
            this.previousTabButton.Click += new System.EventHandler(this.previousTabButton_Click);
            // 
            // tabIndexLabel
            // 
            this.tabIndexLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tabIndexLabel.Location = new System.Drawing.Point(336, 325);
            this.tabIndexLabel.Name = "tabIndexLabel";
            this.tabIndexLabel.Size = new System.Drawing.Size(30, 13);
            this.tabIndexLabel.TabIndex = 12;
            this.tabIndexLabel.Text = "Tab:";
            this.tabIndexLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rowCountComboBox
            // 
            this.rowCountComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rowCountComboBox.DisplayMember = "1";
            this.rowCountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rowCountComboBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rowCountComboBox.FormattingEnabled = true;
            this.rowCountComboBox.Items.AddRange(new object[] {
            "5",
            "10",
            "20",
            "50",
            "100"});
            this.rowCountComboBox.Location = new System.Drawing.Point(285, 319);
            this.rowCountComboBox.Name = "rowCountComboBox";
            this.rowCountComboBox.Size = new System.Drawing.Size(50, 21);
            this.rowCountComboBox.TabIndex = 11;
            this.rowCountComboBox.ValueMember = "1";
            this.rowCountComboBox.SelectedValueChanged += new System.EventHandler(this.rowCountComboBox_SelectedValueChanged);
            // 
            // rowCountLabel
            // 
            this.rowCountLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rowCountLabel.Location = new System.Drawing.Point(244, 325);
            this.rowCountLabel.Name = "rowCountLabel";
            this.rowCountLabel.Size = new System.Drawing.Size(40, 13);
            this.rowCountLabel.TabIndex = 10;
            this.rowCountLabel.Text = "Rows:";
            this.rowCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // usersGroupBox
            // 
            this.usersGroupBox.Controls.Add(this.tabIndexesComboBox);
            this.usersGroupBox.Controls.Add(this.followingTabButton);
            this.usersGroupBox.Controls.Add(this.usersDataGridView);
            this.usersGroupBox.Controls.Add(this.previousTabButton);
            this.usersGroupBox.Controls.Add(this.rowCountLabel);
            this.usersGroupBox.Controls.Add(this.tabIndexLabel);
            this.usersGroupBox.Controls.Add(this.rowCountComboBox);
            this.usersGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usersGroupBox.Location = new System.Drawing.Point(0, 0);
            this.usersGroupBox.Name = "usersGroupBox";
            this.usersGroupBox.Size = new System.Drawing.Size(684, 350);
            this.usersGroupBox.TabIndex = 6;
            this.usersGroupBox.TabStop = false;
            this.usersGroupBox.Text = "Users";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userCountToolStripStatusLabel,
            this.tabCountToolStripStatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 439);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(684, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip";
            // 
            // userCountToolStripStatusLabel
            // 
            this.userCountToolStripStatusLabel.Name = "userCountToolStripStatusLabel";
            this.userCountToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // tabCountToolStripStatusLabel
            // 
            this.tabCountToolStripStatusLabel.Name = "tabCountToolStripStatusLabel";
            this.tabCountToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(0, 24);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.FiltersGroupBox);
            this.splitContainer.Panel1MinSize = 40;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.usersGroupBox);
            this.splitContainer.Panel2MinSize = 350;
            this.splitContainer.Size = new System.Drawing.Size(684, 415);
            this.splitContainer.SplitterDistance = 60;
            this.splitContainer.SplitterWidth = 12;
            this.splitContainer.TabIndex = 9;
            // 
            // UsersList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(700, 500);
            this.Name = "UsersList";
            this.Text = "Users List";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UsersList_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.usersDataGridView)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.FiltersGroupBox.ResumeLayout(false);
            this.usersGroupBox.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataGridView usersDataGridView;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem openDataToolStripMenuItem;
        private ToolStripMenuItem exportDataToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem exitAndSaveChangesToolStripMenuItem;
        private ToolStripMenuItem exitChangesToolStripMenuItem;
        private MenuStrip menuStrip1;
        private GroupBox FiltersGroupBox;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private ComboBox rowCountComboBox;
        private Label rowCountLabel;
        private Label tabIndexLabel;
        private GroupBox usersGroupBox;
        private ToolStripMenuItem importDataToolStripMenuItem;
        private Button followingTabButton;
        private Button previousTabButton;
        private ComboBox tabIndexesComboBox;
        private RichTextBox filterTextBox;
        private ToolStripSeparator toolStripSeparator2;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel userCountToolStripStatusLabel;
        private ToolStripStatusLabel tabCountToolStripStatusLabel;
        private ToolStripMenuItem saveToolStripMenuItem;
        private SplitContainer splitContainer;
        private DataGridViewTextBoxColumn ID;
        private DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn LastNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn mailDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn numberDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn Town;
        private DataGridViewTextBoxColumn Street;
        private DataGridViewTextBoxColumn HouseNumber;
        private DataGridViewTextBoxColumn HouseNumberSecond;
        private DataGridViewButtonColumn Edit;
        private DataGridViewButtonColumn Delete;
        private ListBox listBox1;
    }
}

