﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Common.Constants;
using Data.Model;
using Data.ViewMode;

namespace TestProject
{
    public partial class UserSettings : Form
    {
        private UserViewModelForEditing UserViewModel { get; }

        public UserSettings(UserModel userModel)
        {
            UserViewModel = new UserViewModelForEditing(userModel);
            InitializeComponent();

            GetAllDataFromUser(UserViewModel);

            ValidateSearchButton(okButton);
        }

        private void GetAllDataFromUser(UserViewModelForEditing userViewModel)
        {
            firstNameMaskedTextBox.Text = userViewModel?.FirstName;
            LastNameMaskedTextBox.Text = userViewModel?.LastName;
            emailMaskedTextBox.Text = userViewModel?.Mail;
            phoneNumberMaskedTextBox.Text = userViewModel?.Number;
            
            townMaskedTextBox.Text = userViewModel?.Town;
            streetMaskedTextBox.Text = userViewModel?.Street;
            houseNumberMaskedTextBox.Text = userViewModel?.HouseNumber;
            houseNumberSubMaskedTextBox.Text = userViewModel?.HouseNumberSecond;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;

            SetAllDataToUser();

            Close();
        }

        private void SetAllDataToUser()
        {
            UserViewModel.Model.FullName = new FullNameModel(firstNameMaskedTextBox.Text, 
                LastNameMaskedTextBox.Text);

            UserViewModel.Model.Address = new UserAddressModel
            {
                TownName = townMaskedTextBox.Text,
                StreetName = streetMaskedTextBox.Text,
                HouseNumber = houseNumberMaskedTextBox.Text,
                HouseNumberSecond = houseNumberSubMaskedTextBox.Text
            };

            UserViewModel.Model.Email = new EmailModel(emailMaskedTextBox.Text);

            UserViewModel.Model.PhoneNumber = new PhoneNumberModel(phoneNumberMaskedTextBox.Text);
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;

            Close();
        }

        private static bool IsTextBoxValid(object sender, string pattern)
        {
            var textBox = sender as MaskedTextBox;
            if (textBox == null)
            {
                throw new ArgumentException(nameof(textBox));
            }

            var text = textBox.Text.Trim();

            var reg = new Regex(pattern);
            if (reg.IsMatch(text))
            {
                textBox.BackColor = Color.White;

                return true;
            }

            textBox.BackColor = Color.LightCoral;

            return false;
        }

        private void ValidateSearchButton(Control buttonToValidate)
        {
            var firstNameTextBox = firstNameMaskedTextBox;
            var LastNameTextBox = LastNameMaskedTextBox;
            var emailTextBox = emailMaskedTextBox;
            var phoneTextBox = phoneNumberMaskedTextBox;
            var towntextBox = townMaskedTextBox;
            var streetTextBox = streetMaskedTextBox;
            var housNumberTextBox = houseNumberMaskedTextBox;
            var housNumberSubTextBox = houseNumberSubMaskedTextBox;

            var result = true;

            result &= IsTextBoxValid(firstNameTextBox, RegexPatternStringConstants.UserName);
            result &= IsTextBoxValid(LastNameTextBox, RegexPatternStringConstants.UserName);
            result &= IsTextBoxValid(emailTextBox, RegexPatternStringConstants.Email);
            result &= IsTextBoxValid(phoneTextBox, RegexPatternStringConstants.PhoneNumber);
            result &= IsTextBoxValid(towntextBox, RegexPatternStringConstants.UserName);
            result &= IsTextBoxValid(streetTextBox, RegexPatternStringConstants.UserName);
            result &= IsTextBoxValid(housNumberTextBox, RegexPatternStringConstants.HouseNumber);
            result &= IsTextBoxValid(housNumberSubTextBox, RegexPatternStringConstants.HouseNumberSecond);

            buttonToValidate.Enabled = result;
        }

        private void firstNameMaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void LastNameMaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void townMaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void streetMaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void maskedTextBox1_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void maskedTextBox2_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void emailMaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }

        private void phoneNumberMaskedTextBox_TextChanged(object sender, EventArgs e)
        {
            ValidateSearchButton(okButton);
        }
    }
}
