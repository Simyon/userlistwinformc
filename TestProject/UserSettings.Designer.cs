﻿using System.ComponentModel;
using System.Windows.Forms;

namespace TestProject
{
    partial class UserSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fullNameGroupBox = new System.Windows.Forms.GroupBox();
            this.LastNameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.firstNameMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.firstNameLabel = new System.Windows.Forms.Label();
            this.addressGroupBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.houseNumberSubMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.houseNumberlabel = new System.Windows.Forms.Label();
            this.houseNumberMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.streetLabel = new System.Windows.Forms.Label();
            this.townLabel = new System.Windows.Forms.Label();
            this.streetMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.townMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.otherGroupBox = new System.Windows.Forms.GroupBox();
            this.phoneNumberLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.phoneNumberMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.emailMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.cancel = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.fullNameGroupBox.SuspendLayout();
            this.addressGroupBox.SuspendLayout();
            this.otherGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // fullNameGroupBox
            // 
            this.fullNameGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fullNameGroupBox.Controls.Add(this.LastNameMaskedTextBox);
            this.fullNameGroupBox.Controls.Add(this.firstNameMaskedTextBox);
            this.fullNameGroupBox.Controls.Add(this.LastNameLabel);
            this.fullNameGroupBox.Controls.Add(this.firstNameLabel);
            this.fullNameGroupBox.Location = new System.Drawing.Point(12, 12);
            this.fullNameGroupBox.Name = "fullNameGroupBox";
            this.fullNameGroupBox.Size = new System.Drawing.Size(450, 60);
            this.fullNameGroupBox.TabIndex = 0;
            this.fullNameGroupBox.TabStop = false;
            this.fullNameGroupBox.Text = "Full Name";
            // 
            // LastNameMaskedTextBox
            // 
            this.LastNameMaskedTextBox.Location = new System.Drawing.Point(102, 34);
            this.LastNameMaskedTextBox.Name = "LastNameMaskedTextBox";
            this.LastNameMaskedTextBox.Size = new System.Drawing.Size(152, 20);
            this.LastNameMaskedTextBox.TabIndex = 7;
            this.LastNameMaskedTextBox.TextChanged += new System.EventHandler(this.LastNameMaskedTextBox_TextChanged);
            // 
            // firstNameMaskedTextBox
            // 
            this.firstNameMaskedTextBox.Location = new System.Drawing.Point(102, 12);
            this.firstNameMaskedTextBox.Name = "firstNameMaskedTextBox";
            this.firstNameMaskedTextBox.Size = new System.Drawing.Size(152, 20);
            this.firstNameMaskedTextBox.TabIndex = 6;
            this.firstNameMaskedTextBox.TextChanged += new System.EventHandler(this.firstNameMaskedTextBox_TextChanged);
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.Location = new System.Drawing.Point(6, 38);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(90, 13);
            this.LastNameLabel.TabIndex = 5;
            this.LastNameLabel.Text = "Last Name:";
            this.LastNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.Location = new System.Drawing.Point(6, 16);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(90, 13);
            this.firstNameLabel.TabIndex = 4;
            this.firstNameLabel.Text = "First Name:";
            this.firstNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // addressGroupBox
            // 
            this.addressGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addressGroupBox.Controls.Add(this.label1);
            this.addressGroupBox.Controls.Add(this.houseNumberSubMaskedTextBox);
            this.addressGroupBox.Controls.Add(this.houseNumberlabel);
            this.addressGroupBox.Controls.Add(this.houseNumberMaskedTextBox);
            this.addressGroupBox.Controls.Add(this.streetLabel);
            this.addressGroupBox.Controls.Add(this.townLabel);
            this.addressGroupBox.Controls.Add(this.streetMaskedTextBox);
            this.addressGroupBox.Controls.Add(this.townMaskedTextBox);
            this.addressGroupBox.Location = new System.Drawing.Point(12, 78);
            this.addressGroupBox.Name = "addressGroupBox";
            this.addressGroupBox.Size = new System.Drawing.Size(450, 60);
            this.addressGroupBox.TabIndex = 1;
            this.addressGroupBox.TabStop = false;
            this.addressGroupBox.Text = "Address";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(266, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "House PhoneNumber Sub:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // houseNumberSubMaskedTextBox
            // 
            this.houseNumberSubMaskedTextBox.Location = new System.Drawing.Point(392, 36);
            this.houseNumberSubMaskedTextBox.Name = "houseNumberSubMaskedTextBox";
            this.houseNumberSubMaskedTextBox.Size = new System.Drawing.Size(50, 20);
            this.houseNumberSubMaskedTextBox.TabIndex = 15;
            this.houseNumberSubMaskedTextBox.TextChanged += new System.EventHandler(this.maskedTextBox2_TextChanged);
            // 
            // houseNumberlabel
            // 
            this.houseNumberlabel.Location = new System.Drawing.Point(266, 17);
            this.houseNumberlabel.Name = "houseNumberlabel";
            this.houseNumberlabel.Size = new System.Drawing.Size(120, 13);
            this.houseNumberlabel.TabIndex = 12;
            this.houseNumberlabel.Text = "House PhoneNumber:";
            this.houseNumberlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // houseNumberMaskedTextBox
            // 
            this.houseNumberMaskedTextBox.Location = new System.Drawing.Point(392, 13);
            this.houseNumberMaskedTextBox.Name = "houseNumberMaskedTextBox";
            this.houseNumberMaskedTextBox.Size = new System.Drawing.Size(50, 20);
            this.houseNumberMaskedTextBox.TabIndex = 13;
            this.houseNumberMaskedTextBox.TextChanged += new System.EventHandler(this.maskedTextBox1_TextChanged);
            // 
            // streetLabel
            // 
            this.streetLabel.Location = new System.Drawing.Point(6, 38);
            this.streetLabel.Name = "streetLabel";
            this.streetLabel.Size = new System.Drawing.Size(90, 13);
            this.streetLabel.TabIndex = 9;
            this.streetLabel.Text = "Street:";
            this.streetLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // townLabel
            // 
            this.townLabel.Location = new System.Drawing.Point(6, 16);
            this.townLabel.Name = "townLabel";
            this.townLabel.Size = new System.Drawing.Size(90, 13);
            this.townLabel.TabIndex = 8;
            this.townLabel.Text = "Town:";
            this.townLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // streetMaskedTextBox
            // 
            this.streetMaskedTextBox.Location = new System.Drawing.Point(102, 34);
            this.streetMaskedTextBox.Name = "streetMaskedTextBox";
            this.streetMaskedTextBox.Size = new System.Drawing.Size(152, 20);
            this.streetMaskedTextBox.TabIndex = 11;
            this.streetMaskedTextBox.TextChanged += new System.EventHandler(this.streetMaskedTextBox_TextChanged);
            // 
            // townMaskedTextBox
            // 
            this.townMaskedTextBox.Location = new System.Drawing.Point(102, 12);
            this.townMaskedTextBox.Name = "townMaskedTextBox";
            this.townMaskedTextBox.Size = new System.Drawing.Size(152, 20);
            this.townMaskedTextBox.TabIndex = 10;
            this.townMaskedTextBox.TextChanged += new System.EventHandler(this.townMaskedTextBox_TextChanged);
            // 
            // otherGroupBox
            // 
            this.otherGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.otherGroupBox.Controls.Add(this.phoneNumberLabel);
            this.otherGroupBox.Controls.Add(this.emailLabel);
            this.otherGroupBox.Controls.Add(this.phoneNumberMaskedTextBox);
            this.otherGroupBox.Controls.Add(this.emailMaskedTextBox);
            this.otherGroupBox.Location = new System.Drawing.Point(12, 144);
            this.otherGroupBox.Name = "otherGroupBox";
            this.otherGroupBox.Size = new System.Drawing.Size(450, 60);
            this.otherGroupBox.TabIndex = 2;
            this.otherGroupBox.TabStop = false;
            this.otherGroupBox.Text = "Other";
            // 
            // phoneNumberLabel
            // 
            this.phoneNumberLabel.Location = new System.Drawing.Point(6, 38);
            this.phoneNumberLabel.Name = "phoneNumberLabel";
            this.phoneNumberLabel.Size = new System.Drawing.Size(90, 13);
            this.phoneNumberLabel.TabIndex = 9;
            this.phoneNumberLabel.Text = "Phone PhoneNumber:";
            this.phoneNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // emailLabel
            // 
            this.emailLabel.Location = new System.Drawing.Point(6, 16);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(90, 13);
            this.emailLabel.TabIndex = 8;
            this.emailLabel.Text = "Email:";
            this.emailLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // phoneNumberMaskedTextBox
            // 
            this.phoneNumberMaskedTextBox.Location = new System.Drawing.Point(102, 34);
            this.phoneNumberMaskedTextBox.Name = "phoneNumberMaskedTextBox";
            this.phoneNumberMaskedTextBox.Size = new System.Drawing.Size(152, 20);
            this.phoneNumberMaskedTextBox.TabIndex = 11;
            this.phoneNumberMaskedTextBox.TextChanged += new System.EventHandler(this.phoneNumberMaskedTextBox_TextChanged);
            // 
            // emailMaskedTextBox
            // 
            this.emailMaskedTextBox.Location = new System.Drawing.Point(102, 12);
            this.emailMaskedTextBox.Name = "emailMaskedTextBox";
            this.emailMaskedTextBox.Size = new System.Drawing.Size(152, 20);
            this.emailMaskedTextBox.TabIndex = 10;
            this.emailMaskedTextBox.TextChanged += new System.EventHandler(this.emailMaskedTextBox_TextChanged);
            // 
            // cancel
            // 
            this.cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel.Location = new System.Drawing.Point(388, 216);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 3;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.Location = new System.Drawing.Point(307, 216);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 4;
            this.okButton.Text = "Ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // UserSettings
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancel;
            this.ClientSize = new System.Drawing.Size(474, 251);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.otherGroupBox);
            this.Controls.Add(this.addressGroupBox);
            this.Controls.Add(this.fullNameGroupBox);
            this.MaximumSize = new System.Drawing.Size(490, 290);
            this.MinimumSize = new System.Drawing.Size(490, 290);
            this.Name = "UserSettings";
            this.Text = "Edit UserModel Settings";
            this.fullNameGroupBox.ResumeLayout(false);
            this.fullNameGroupBox.PerformLayout();
            this.addressGroupBox.ResumeLayout(false);
            this.addressGroupBox.PerformLayout();
            this.otherGroupBox.ResumeLayout(false);
            this.otherGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox fullNameGroupBox;
        private GroupBox addressGroupBox;
        private GroupBox otherGroupBox;
        private Button cancel;
        private Button okButton;
        private MaskedTextBox LastNameMaskedTextBox;
        private MaskedTextBox firstNameMaskedTextBox;
        private Label LastNameLabel;
        private Label firstNameLabel;
        private Label label1;
        private MaskedTextBox houseNumberSubMaskedTextBox;
        private Label houseNumberlabel;
        private MaskedTextBox houseNumberMaskedTextBox;
        private Label streetLabel;
        private Label townLabel;
        private MaskedTextBox streetMaskedTextBox;
        private MaskedTextBox townMaskedTextBox;
        private Label phoneNumberLabel;
        private Label emailLabel;
        private MaskedTextBox phoneNumberMaskedTextBox;
        private MaskedTextBox emailMaskedTextBox;
    }
}