﻿using System;
using System.Windows.Forms;

namespace TestProject
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(params string[] args)
        {
            var fullFileName = string.Empty;
            var fullFilterSettings = string.Empty;

            if (args.Length > 0)
            {
                fullFileName = args[0];
            }

            if (args.Length > 1)
            {
                fullFilterSettings = args[1];
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UsersList(fullFileName, fullFilterSettings));
        }
    }
}
