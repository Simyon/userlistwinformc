﻿using System.Collections.Generic;

namespace Common.Constants
{
    public static class ArraStringConstants
    {
        public static readonly List<string> AutoCompliteVariants = new List<string>
        {
            "f",
            "fn",
            "l",
            "ln",
            "t",
            "tn",
            "s",
            "sn",
            "n",
            "p",
            "pn",
            "h",
            "hn",
            "hns",
            "hs",
            "e",
            "first",
            "firstname",
            "last",
            "lastname",
            "town",
            "townname",
            "street",
            "streetname",
            "name",
            "phone",
            "phonenumber",
            "email",
            "number",
            "second",
            "house",
            "housenumber",
            "housenumbersecond",
        };

        public static readonly List<string> AutoCompliteVariantsPart = new List<string>
        {
            ".p",
            ".part",
            ".c",
            ".cs",
            ".s",
            ".case",
            ".casesensetive",
            ".sensetive"
        };
    }
}
