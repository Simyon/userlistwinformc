﻿namespace Common.Constants
{
    public static class ValidationStringConstant
    {
        public const string MustBeNotEmpty = "Must be not empty";

        public const string DoesntCorrespondToPattern= "Doesn't correspond to pattern";
    }
}
