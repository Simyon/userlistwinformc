﻿namespace Common.Constants
{
    public static class QuestionStringConstants
    {
        public const string TryToDeleteMessageText = "Нou are sure that you want to remove user?";

        public const string TryToCloseMessageText =
            "You try to close application, but you didn't save your progress. Do you want to save it before exit?";

        public const string TryToOpenNewMessageText =
            "You try to open new users list, but you didn't save your progress. Do you want to save it before open?";
    }
}
