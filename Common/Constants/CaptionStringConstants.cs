﻿namespace Common.Constants
{
    public static class CaptionStringConstants
    {
        public const string Question = "Question";

        public const string Information = "Information";

        public const string Error = "Error";
    }
}
