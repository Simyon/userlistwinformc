﻿namespace Common.Constants
{
    public static class CommandStringConstants
    {
        public const string Edit = "Edit";

        public const string Delete = "Delete";

        public const string Create = "Create";

        public const string CreateFromThis = "CreateFromThis";

        public const string Open = "Open";

        public const string Save = "Save";

        public const string Import = "Import";

        public const string Export = "Export";
    }
}
