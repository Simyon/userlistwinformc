﻿namespace Common.Constants
{
    public static class RegexPatternStringConstants
    {
        public const string Email = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";

        public const string UserName = "^[a-zA-Z]{2,30}$";

        public const string HouseNumber = "^[0-9]{1,3}$";

        public const string HouseNumberSecond = "^[a-zA-Z0-9]{0,3}$";

        public const string PhoneNumber = "^[0-9]{6,10}$";

        public const string FullCommand = @"^(\s*(first|f)\s*(name|n)?\s*|\s*(last|l)\s*(name|n)?\s*|\s*(town|t)\s*(name|n)?\s*|\s*(street|s)\s*(name|n)?\s*|\s*(phone|p)\s*(number|n)?\s*|\s*(house|h)\s*(number|n)?s*|\s*(house|h)\s*(number|n)?\s*(second|s)\s*|\s*(email|e)\s*)(\.\s*((part|p)|(case|c)\s*(sensetive|s)?)\s*){0,2}\s*=\s*.{1,30}\s*$";

        public const string SimpleCommand = @"^\s*[a-zA-Z](\s*\.\s*[a-zA-Z]){0,2}\s*=\s*.{1,30};$";

        public const string FirstNameCommandPart = @"^\s*(first|f)\s*(name|n)?\s*$";

        public const string LastNameCommandPart = @"^\s*(last|l)\s*(name|n)?\s*$";

        public const string TownNameCommandPart = @"^\s*(town|t)\s*(name|n)?\s*$";

        public const string StreetNameCommandPart = @"^\s*(street|s)\s*(name|n)?\s*$";

        public const string HouseNumberCommandPart = @"^\s*(house|h)\s*(number|n)?\s*$";

        public const string HouseNumberSecondCommandPart = @"^\s*(house|h)\s*(number|n)?\s*(second|s)\s*$";

        public const string PhoneNumberCommandPart = @"^\s*(phone|p)\s*(number|n)?\s*$";

        public const string EmailCommandPart = @"^\s*(email|e)\s*$";

        public const string IsPartCommandPart = @"^(\s*(part|p)\s*)$";

        public const string CaseSensetiveCommandPart = @"^(\s*(case|c)\s*(sensetive|s)?\s*)$";
    }
}