﻿namespace Common.Constants
{
    public static class PropertyNameStringConstants
    {
        public const string FirstName = "FirstName";

        public const string LastName = "LastName";

        public const string TownName = "TownName";

        public const string StreetName = "StreetName";

        public const string HouseNumber = "HouseNumber";

        public const string HouseNumberSecond = "HouseNumberSecond";

        public const string Email = "Email";

        public const string PhoneNumber = "PhoneNumber";
    }
}
