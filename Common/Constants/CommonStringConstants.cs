﻿namespace Common.Constants
{
    public static class CommonStringConstants
    {
        public const string DaublePoint = ": ";

        public const string FileFilterConstant = "Users Data (xml)|*.xml";

        public const string TabCount = "Tab Count";

        public const string UserCount = "User Count";

        public const string Hint = "Write like-> [param]=[value]; Press CTRL+Space for get auto complite";
    }
}
