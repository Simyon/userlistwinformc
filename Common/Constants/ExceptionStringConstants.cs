﻿namespace Common.Constants
{
    public static class ExceptionStringConstants
    {
        public const string FileNotExist = "File is not Exist";

        public const string FileEmpty = "File is empty";

        public const string CanNotParseXml = "Can't parse XML";

        public const string CanNotParseUserId = "Can't parse user ID";

        public const string NotUniqueIDs = "ID's not unique";

        public const string DuplicatedIdentifiers = "Duplicated identifiers in data";

        public const string UncorrectCommand = "Uncorrect command";
    }
}
