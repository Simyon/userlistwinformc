﻿using System.Drawing;

namespace Common.Constants
{
    public static class ColorConstants
    {
        public static readonly Color NotValidBackColor = Color.LightCoral;

        public static readonly Color ValidBackColor = Color.White;

        public static readonly Color HintForeColor = Color.Gray;

        public static readonly Color NormalForeColor = Color.Black;

        public static readonly Color CompleteCommandForeColor = Color.Green;
        public static readonly Color OnAutoComplete = Color.DimGray ;
    }
}
