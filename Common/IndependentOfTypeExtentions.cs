﻿using Common.Serializers;

namespace Common
{
    public static class IndependentOfTypeExtentions
    {
        public static T DeepClone<T>(T obj)
        {
            var jsonUser = obj.ToJson();

            return jsonUser.FromJson<T>();
        }
    }
}
