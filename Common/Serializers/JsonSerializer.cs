﻿using Newtonsoft.Json;

namespace Common.Serializers
{
    public static class JsonSerializer
    {
        private static readonly JsonSerializerSettings SerializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto
        };
        
        /// <summary>
        /// Получить объект из Json строки
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objString"></param>
        /// <returns></returns>
        public static T FromJson<T>(this string objString)
        {
            return JsonConvert.DeserializeObject<T>(objString, SerializerSettings);
        }

        /// <summary>
        /// Получить строку Json из объекта
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, SerializerSettings);
        }
    }
}
