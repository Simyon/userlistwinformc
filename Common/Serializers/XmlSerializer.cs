﻿using System.IO;
using System.Text;
using System.Xml;

namespace Common.Serializers
{
    public static class XmlSerializer
    {
        public static bool TryParseXml<T>(this string objString, out T result) where T : new()
        {
            result = default(T);

            try
            {
                result = objString.FromXml<T>();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string ToXml<T>(this T value)
        {
            using (var textWriter = new StringWriter())
            {
                var settings = new XmlWriterSettings
                {
                    Encoding = new UnicodeEncoding(false, false),
                    Indent = false,
                    OmitXmlDeclaration = false
                };

                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                    serializer.Serialize(xmlWriter, value);
                }

                return textWriter.ToString();
            }
        }

        public static T FromXml<T>(this string xml)
        {
            using (var textReader = new StringReader(xml))
            {
                var settings = new XmlReaderSettings();
                using (var xmlReader = XmlReader.Create(textReader, settings))
                {
                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));

                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }
    }
}
