﻿using System;
using System.IO;
using Common.Constants;

namespace Common.Serializers
{
    /// <summary>
    /// 
    /// </summary>
    public static class XmlFileWorker
    {
        /// <summary>
        /// Получить объект из Xml файла
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fullFileName"></param>
        /// <returns></returns>
        public static T ReadXml<T>(string fullFileName) where  T : new()
        {
            if (!File.Exists(fullFileName))
            {
                throw new Exception(ExceptionStringConstants.FileNotExist);
            }

            T result;

            using (var fs = new FileStream(fullFileName, FileMode.Open))
            {
                var reader = new StreamReader(fs);
                var str = reader.ReadToEnd();
                if (string.IsNullOrWhiteSpace(str))
                {
                    throw new Exception(ExceptionStringConstants.FileEmpty);
                }

                var isParsed = str.TryParseXml(out result);
                if (!isParsed)
                {
                    throw new Exception(ExceptionStringConstants.CanNotParseXml);
                }
            }

            return result;
        }

        /// <summary>
        /// Записать объект в Xml файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fullFileName"></param>
        public static void WriteXml<T>(this T obj,
            string fullFileName)
        {
            var formatter = new System.Xml.Serialization.XmlSerializer(typeof(T));
            using (var fs = new FileStream(fullFileName, FileMode.Create))
            {
                formatter.Serialize(fs, obj);
            }
        }
    }
}
